# Grdtools

## Build

We use girderstream to build the tools, it need buildbox-casd to be running

```bash
buildbox-casd  --bind localhost:50040 ~/.cache/girderstream/cas
```

Then we can use girderstream to build a element

```bash
girderstream build --name base-bootstrap
```

Then we can look at any build logs

```bash
girderstream export-logs --name base-bootstrap --location logs
cat logs/gcc-pass1.stderr 
```

You can look at what has been built
```bash
girderstream checkout --name file-pass1 --location ./file
tree ./file
ls file/usr/share/misc/magic.mgc  -lh
```

## Test

Once a element is built we can shell into it

```bash
❯ girderstream shell --name base-bootstrap
unbuilt junctions: []
built junctions
Debugging shell:
sh-5.2# ls
bin  dev  etc  include	lib  lib64  proc  tmp  tools  usr  var	x86_64-lfs-linux-gnu
```

## Extended test

### Test aarch64 cross compilers

These tests should be manged properly, and may be moved to another repo.

We can build with the cross compilers

```bash
girderstream --options arch=aarch64 build --name filesystem
```

And then check out the built elements

```bash
girderstream --options arch=aarch64 checkout --name filesystem --location ./fs
girderstream --options arch=aarch64 checkout --name kernel --location ./kernel
```

And then run the cross compiled build

```bash
qemu-system-aarch64 -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel kernel/boot/Image -drive file=fs/disk.img,format=raw -append "root=/dev/vda2 rw"
```

Which will boot into the cross built system
```
[    1.143754] EXT4-fs (vda2): mounted filesystem with ordered data mode. Quota mode: none.
[    1.145033] VFS: Mounted root (ext4 filesystem) on device 254:2.
[    1.149541] devtmpfs: error mounting -2
[    1.247690] Freeing unused kernel memory: 6976K
[    1.248545] Run /sbin/init as init process
/bin/sh: can't access tty; job control turned off
/ # ls
bin         etc         lost+found  sbin        var
boot        lib         opt         sys
dev         linuxrc     proc        usr
/ # ls /proc/
1              27             5              driver         mtd
[...]
```

### Test x86_64 cross compilers

We can build native elements with the cross compilers. This allows us to decouple the kernel and
base libs from those used on the build system.

```bash
girderstream --options arch=x86_64 build --name filesystem
```

And then check out the built elements

We can build with the cross compilers
```bash
girderstream --options arch=x86_64 checkout --name filesystem --location ./fs_x86
girderstream --options arch=x86_64 checkout --name kernel --location ./lx_x86
```

And then run the cross compiled build
```bash
qemu-system-x86_64 -nographic -smp 2 -m 4096 -kernel lx_x86/boot/bzImage -drive file=fs_x86/disk.img,format=raw -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"
```

## SDK layout

### SDK

The sdk has all of its libs in /usr/lib with links from /lib/ and /lib64 to /usr/lib

The sdk also has /usr/bin on its path with /bin/ a link to /usr/bin.

```bash
❯ girderstream --options=arch=x86_64 shell --name sdk-cross-autotools
unbuilt junctions: []
built junctions
Debugging shell:
sh-5.2# ls -l
total 0
lrwxrwxrwx   1     0     0   8 Nov 11  2011 bin -> /usr/bin
drwxr-xr-x   4     0     0 340 Nov 18 21:34 dev
drwxr-xr-x   1     0     0   0 Nov 11  2011 etc
lrwxrwxrwx   1     0     0   8 Nov 11  2011 lib -> /usr/lib
lrwxrwxrwx   1     0     0   8 Nov 11  2011 lib64 -> /usr/lib
drwxr-xr-x   1     0     0   0 Nov 11  2011 opt
dr-xr-xr-x 579 65534 65534   0 Nov 18 21:34 proc
drwxr-xr-x   2     0     0  40 Nov 18 21:34 tmp
drwxr-xr-x   1     0     0   0 Nov 11  2011 usr
drwxr-xr-x   1     0     0   0 Nov 11  2011 var
sh-5.2# ls -l /usr/
total 0
drwxr-xr-x 1 0 0 0 Nov 11  2011 bin
drwxr-xr-x 1 0 0 0 Nov 11  2011 etc
drwxr-xr-x 1 0 0 0 Nov 11  2011 include
drwxr-xr-x 1 0 0 0 Nov 11  2011 lib
drwxr-xr-x 1 0 0 0 Nov 11  2011 libexec
drwxr-xr-x 1 0 0 0 Nov 11  2011 sbin
drwxr-xr-x 1 0 0 0 Nov 11  2011 share

```

### Target

We create a example target in the test-* elements to show that our cross compilers can be used sensibly but this should be extended in target projects.

This creates a target with a consistent layout for aarch64 and x86_64

This is consistent for aarch64 and x86_64

```bash
/ # ls -l
total 44
drwxr-xr-x    2 root     root          4096 Nov 11  2011 bin
drwxr-xr-x    2 root     root          4096 Nov 11  2011 boot
drwxr-xr-x    3 root     root            60 Nov 18 21:28 dev
drwxr-xr-x    3 root     root          4096 Nov 18 18:38 etc
lrwxrwxrwx    1 root     root             8 Nov 18 18:38 lib -> /usr/lib
lrwxrwxrwx    1 root     root             8 Nov 18 18:38 lib64 -> /usr/lib
lrwxrwxrwx    1 root     root            11 Nov 11  2011 linuxrc -> bin/busybox
drwx------    2 root     root         16384 Nov 18 18:38 lost+found
drwxr-xr-x    3 root     root          4096 Nov 11  2011 opt
dr-xr-xr-x  110 root     root             0 Nov 18 21:28 proc
drwxr-xr-x    2 root     root          4096 Nov 11  2011 sbin
dr-xr-xr-x   12 root     root             0 Nov 18 21:28 sys
drwxr-xr-x    6 root     root          4096 Nov 18 18:38 usr
drwxr-xr-x    3 root     root          4096 Nov 11  2011 var
/ # ls -l /usr/
total 16
drwxr-xr-x    2 root     root          4096 Nov 11  2011 bin
drwxr-xr-x    6 root     root          4096 Nov 11  2011 lib
drwxr-xr-x    3 root     root          4096 Nov 11  2011 libexec
drwxr-xr-x    2 root     root          4096 Nov 11  2011 sbin
```

The /bin/ /usr/bin/ and /sbin/ are all provided by busybox but a gnu base userland might also be added in a target project.

