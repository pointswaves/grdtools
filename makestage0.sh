#!/bin/bash

set -xe

# use debootstrap to create a base layer.
# build and cache girderstream-{build,source}-plugin
# update digests.

if [ ! -d stage0 ]; then
    mkdir stage0
    sudo FAKECHROOT=true debootstrap --arch amd64 --include build-essential,texinfo,gawk,bison,python3,libboost-regex1.74.0,rust-all,git,tar,ca-certificates,wget,xz-utils,file bookworm stage0/ http://deb.debian.org/debian/
    sudo rm -rf stage0/dev
    sudo rm -f stage0/bin/sh
    sudo ln -s /usr/bin/bash stage0/bin/sh
    sudo rm -f stage0/etc/resolv.conf
    echo "nameserver 8.8.8.8" | sudo tee stage0/etc/resolv.conf
fi
sudo casupload --cas-server=http://localhost:50040 --output-digest-file=digest stage0
DIGEST=$(cat digest);
sed -e "s#<DIGEST>#${DIGEST}#" \
    elements/tarball.grd.in > elements/tarball.grd
girderstream -v build --name tarball.grd

girderstream -v build --name binutils-pass1.grd
